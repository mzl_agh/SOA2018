﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUDClient.ObjectsClient;

namespace CRUDClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectsServiceClient client = new ObjectsServiceClient();

            //############## This will add authors to database
            //var authors = new[]
            //{
            //    new Author() {Name = "Jane", Surname = "Kasinsky"},
            //    new Author() {Name = "Michell", Surname = "Nowakowsky"},
            //    new Author() {Name = "Veronica", Surname = "Veri"},
            //    new Author() {Name = "Jack", Surname = "Joffry"}
            //};

            //foreach (var author in authors)
            //{
            //    author.Id = client.AddAuthor(author);
            //}
            
            //############## This will update one of authors
            //var author = client.GetAllAuthors().FirstOrDefault(x => x.Name == "Veronica" && x.Surname == "Veri");
            //author.Surname = "Staton";
            //client.UpdateAuthor(author);

            //var newAuth = client.GetAuthor(author.Id);
            //Console.WriteLine("Name: {0} Surname: {1}", newAuth.Name, newAuth.Surname);

            //############## This will create two new authors and create comics with them
            //var comics = new Comics()
            //{
            //    Title = "Amazing Coderman",
            //    Authors =
            //        new Author[] { new Author() { Name = "Jane", Surname = "CodeGirl" }, new Author() { Name = "Sam", Surname = "Geeky" } }
            //};


            //comics.Authors.ToList().ForEach(x => x.Id = client.AddAuthor(x));
            //comics.Id = client.AddComics(comics);

            //comics = client.GetComics(comics.Id);
            //Console.WriteLine("Title: {0}", comics.Title);
            //foreach (var author in comics.Authors)
            //{
            //    Console.WriteLine("\tAuthor: {0} {1}", author.Name, author.Surname);
            //}

            //############## Delete comics with title
            //var comics = client.GetAllComicses().FirstOrDefault(x => x.Title.ToLower().StartsWith("amazing"));
            //var success = client.DeleteComics(comics.Id);

            Console.ReadKey();
        }
    }
}
