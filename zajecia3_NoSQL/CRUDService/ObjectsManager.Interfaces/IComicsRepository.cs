﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectsManager.Model;

namespace ObjectsManager.Interfaces
{
    public interface IComicsRepository
    {
        List<Comics> GetAll();
        int Add(Comics comics);
        Comics Get(int id);
        Comics Update(Comics comics);
        bool Delete(int id);      
    }
}
