﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ObjectsManager.Interfaces;
using ObjectsManager.LiteDB;

namespace CRUDService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ObjectsService : IObjectsService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IComicsRepository _comicsRepository;

        public ObjectsService()
        {
            this._authorRepository = new AuthorRepository();
            this._comicsRepository = new ComicsRepository();
        }
        public int AddComics(ObjectsManager.Model.Comics comics)
        {
            return this._comicsRepository.Add(comics);
        }

        public ObjectsManager.Model.Comics GetComics(int id)
        {
            return this._comicsRepository.Get(id);
        }

        public List<ObjectsManager.Model.Comics> GetAllComicses()
        {
            return this._comicsRepository.GetAll();
        }

        public ObjectsManager.Model.Comics UpdateComics(ObjectsManager.Model.Comics comics)
        {
            return this._comicsRepository.Update(comics);
        }

        public bool DeleteComics(int id)
        {
            return this._comicsRepository.Delete(id);
        }

        public int AddAuthor(ObjectsManager.Model.Author author)
        {
            return this._authorRepository.Add(author);
        }

        public ObjectsManager.Model.Author GetAuthor(int id)
        {
            return this._authorRepository.Get(id);
        }

        public List<ObjectsManager.Model.Author> GetAllAuthors()
        {
            return this._authorRepository.GetAll();
        }

        public ObjectsManager.Model.Author UpdateAuthor(ObjectsManager.Model.Author author)
        {
            return this._authorRepository.Update(author);
        }

        //public bool DeleteAuthor(int id)
        //{
        //    return this._authorRepository.Delete(id);
        //}
    }
}
