﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using ObjectsManager.Interfaces;
using ObjectsManager.LiteDB.Model;
using ObjectsManager.Model;

namespace ObjectsManager.LiteDB
{
    public class ComicsRepository : IComicsRepository
    {
        private readonly string _comicsConnection = DatabaseConnections.ComicsConnection;

        public List<Comics> GetAll()
        {
            using (var db = new LiteDatabase(this._comicsConnection))
            {
                var repository = db.GetCollection<ComicsDB>("comics");
                var results = repository.FindAll();

                return results.Select(x => Map(x)).ToList();
            }
        }

        public int Add(Comics comics)
        {
            using (var db = new LiteDatabase(this._comicsConnection))
            {
                var dbObject = InverseMap(comics);


                var repository = db.GetCollection<ComicsDB>("comics");
                repository.Insert(dbObject);

                return dbObject.Id;
            }
        }

        public Comics Get(int id)
        {
            using (var db = new LiteDatabase(this._comicsConnection))
            {
                var repository = db.GetCollection<ComicsDB>("comics");
                var result = repository.FindById(id);
                return Map(result);
            }
        }

        public Comics Update(Comics comics)
        {
            using (var db = new LiteDatabase(this._comicsConnection))
            {
                var dbObject = InverseMap(comics);

                var repository = db.GetCollection<ComicsDB>("comics");
                if (repository.Update(dbObject))
                    return Map(dbObject);
                else
                    return null;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(this._comicsConnection))
            {
                var repository = db.GetCollection<ComicsDB>("comics");
                return repository.Delete(id);
            }
        }

        private Comics Map(ComicsDB dbComics)
        {
            if (dbComics == null)
                return null;
            var authRepo = new AuthorRepository();
            var authors = authRepo.GetAuthors(dbComics.AuthorsIds).Select(x => authRepo.Map(x)).ToList();
            return new Comics() {Id = dbComics.Id, Title = dbComics.Title, Authors = authors};
        }

        private ComicsDB InverseMap(Comics comics)
        {
            if (comics == null)
                return null;
            return new ComicsDB()
            {
                Id = comics.Id,
                Title = comics.Title,
                AuthorsIds = comics.Authors.Select(x => x.Id).ToArray()
            };
        }
    }
}
