﻿using ConsoleApplication7.TestRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {
            TestClient testClient = new TestClient();
            Classroom classr = new Classroom();
            classr.People = new Person[] { new Person() { Score = 2 }, new Person() { Score = 3 } };
            foreach (var pers in classr.People.ToList())
            {
                Console.WriteLine(pers.Score);
            }

            var newClass = testClient.SetScore(classr);

            foreach (var pers in newClass.People.ToList())
            {
                Console.WriteLine(pers.Score);
            }
            Console.ReadLine();
        }
    }
}
