﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri address = new Uri("http://localhost:80/Temporary_Listen_Addresses/WCFTest");
            ServiceHost selfHost = new ServiceHost(typeof(Test), address);

            try
            {
                //to pozwala na obsluge takiego punktu koncowego
                selfHost.AddServiceEndpoint(typeof(ITest), new WSHttpBinding(), "TestServiceEndpoint");

                // to pozwala widziec nam wsdl
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                selfHost.Description.Behaviors.Add(smb);

                selfHost.Open();
                Console.WriteLine("Service is working!");
                Console.ReadLine();

                selfHost.Close();
            }
            catch (CommunicationException ex)
            {
                Console.WriteLine("An exception of type CommunicationException occured: {0}", ex.Message);
                selfHost.Abort();
            }
        }
    }

    [ServiceContract]
    public interface ITest
    {
        [OperationContract]
        Classroom SetScore(Classroom classroom);
    }

    public class Test : ITest
    {
        public Classroom SetScore(Classroom classroom)
        {
            foreach (var person in classroom.People)
            {
                Console.WriteLine(person.Score);
                person.Score = 5;
            }
            return classroom;

        }
    }

    public class Person
    {
        //private int _score;
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Score { get; set; }
        //public int Score { get { return this._score; } set { this._score = value + 1; }}
    }

    public class Classroom
    {
        public List<Person> People { get; set; }
    }
}
