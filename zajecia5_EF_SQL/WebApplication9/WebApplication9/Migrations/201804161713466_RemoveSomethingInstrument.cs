namespace WebApplication9.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveSomethingInstrument : DbMigration
    {
        public override void Up()
        {
            DropColumn("public.Instruments", "Something");
        }
        
        public override void Down()
        {
            AddColumn("public.Instruments", "Something", c => c.String());
        }
    }
}
