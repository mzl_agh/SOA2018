namespace WebApplication9.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.Albums",
                c => new
                    {
                        AlbumID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        GenreID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AlbumID)
                .ForeignKey("public.Genres", t => t.GenreID, cascadeDelete: true)
                .Index(t => t.GenreID);
            
            CreateTable(
                "public.Artists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        InstrumentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Instruments", t => t.InstrumentID, cascadeDelete: true)
                .Index(t => t.InstrumentID);
            
            CreateTable(
                "public.Instruments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Rank = c.String(),
                        Something = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "public.Genres",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "public.ArtistAlbums",
                c => new
                    {
                        Artist_ID = c.Int(nullable: false),
                        Album_AlbumID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Artist_ID, t.Album_AlbumID })
                .ForeignKey("public.Artists", t => t.Artist_ID, cascadeDelete: true)
                .ForeignKey("public.Albums", t => t.Album_AlbumID, cascadeDelete: true)
                .Index(t => t.Artist_ID)
                .Index(t => t.Album_AlbumID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.Albums", "GenreID", "public.Genres");
            DropForeignKey("public.Artists", "InstrumentID", "public.Instruments");
            DropForeignKey("public.ArtistAlbums", "Album_AlbumID", "public.Albums");
            DropForeignKey("public.ArtistAlbums", "Artist_ID", "public.Artists");
            DropIndex("public.ArtistAlbums", new[] { "Album_AlbumID" });
            DropIndex("public.ArtistAlbums", new[] { "Artist_ID" });
            DropIndex("public.Artists", new[] { "InstrumentID" });
            DropIndex("public.Albums", new[] { "GenreID" });
            DropTable("public.ArtistAlbums");
            DropTable("public.Genres");
            DropTable("public.Instruments");
            DropTable("public.Artists");
            DropTable("public.Albums");
        }
    }
}
